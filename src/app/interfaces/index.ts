export interface IHttpResponse {
    id: number;
    userName: string;
    token: string;
}