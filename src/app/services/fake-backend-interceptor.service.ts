import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import {
  switchMap,
  catchError
} from 'rxjs/operators';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  constructor(private http: HttpClient) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (req.url.endsWith('/users/authenticate')) {
      // api call
      return this.http.get('http://localhost:8080/api/credentials').pipe(
        switchMap((value: any) => {
          let filteredUsers = value.filter(user => {
            return (
              user.userName === req.body.userName &&
              user.password === req.body.password
            );
          });
          if (filteredUsers.length) {
            // if login details are valid return 200 OK with user details and fake jwt token
            let user = filteredUsers[0];
            let body = {
              id: user.id,
              username: user.userName,
              token: 'fake-jwt-token'
            };
            return of(new HttpResponse({ status: 200, body: body }));
          } else {
            return throwError("UserName and password doesn't match");
          }
        }),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            console.log('Your logging here...');
            let clone = req.clone({ setHeaders: { 'Authorization ': 'test' } });
            return next.handle(clone);
          }

          return throwError(err);
        })
      );
    } else {
      return next.handle(req); // call original auth request.
    }
  }
}

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
