import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(public http: HttpClient) { }

  get currentUserValue() {
   return localStorage.getItem('key');
  }

  set currentUserValue(value: string) {
    localStorage.setItem('key', value);
  }

  login(userName: string, password: string) {
    return this.http.post('/users/authenticate', { userName, password });
  }
}
