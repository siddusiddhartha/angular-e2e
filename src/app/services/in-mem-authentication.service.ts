import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemHeroAuthentication implements InMemoryDbService {
  createDb() {
    let credentials = [
      { id: 1, userName: 'test', password: 'test' },
      { id: 2, userName: 'Bombasto', password: 'test' },
      { id: 3, userName: 'Magneta', password: 'test' },
      { id: 4, userName: 'Tornado', password: 'test' }
    ];
    return {credentials};
  }
}