import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl + '/login') as Promise<any>;
  }

  setUserName(userName: string) {
    return element(by.css('.user-name')).sendKeys(userName) as Promise<void>;
  }

  setPassword(pwd: string) {
    return element(by.css('.password')).sendKeys(pwd) as Promise<void>;
  }

  clearUserName() {
    return element(by.css('.user-name')).clear();
  }

  clearPassword() {
    return element(by.css('.password')).clear();
  }

  clearFields() {
    this.clearUserName();
    this.clearPassword();
  }

  getUserName() {
    return element(by.css('.user-name')).getAttribute('value') as Promise<string>;
  }

  getPassword() {
    return element(by.css('.password')).getAttribute('value') as Promise<string>;
  }

  getErrors() {
    return element.all(by.css('.invalid-feedback')).count();
  }

  submitForm() {
    element(by.css('.btn-login')).click();
  }
}
