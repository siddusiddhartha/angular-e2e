import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-(project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should navigate to login page', () => {
    page.navigateTo();
    expect(browser.getCurrentUrl()).toBe('http://localhost:4200/login');
  });

  it('should show the errors', () => {
    page.submitForm();
    expect(page.getErrors()).toBe(2);
  });

  it('should set the user name', () => {
    page.setUserName('test');
    expect(page.getUserName()).toEqual('test');
  });

  it('should set the password', () => {
    page.setPassword('test');
    expect(page.getPassword()).toEqual('test');
  });

  it('should not navigate to login page', () => {
    page.clearFields();
    page.setUserName('test1');
    page.setPassword('test2');
    page.submitForm();
    expect(browser.getCurrentUrl()).toBe('http://localhost:4200/login');
  });

  it('should navigate to dashboard page' , () => {
    page.clearFields();
    page.setUserName('test');
    page.setPassword('test');
    browser.sleep(5000);
    page.submitForm();
    expect(browser.getCurrentUrl()).toBe('http://localhost:4200/dashboard');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
